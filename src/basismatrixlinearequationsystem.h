#ifndef BASISMATRIXLINEAREQUATIONSYSTEM_H
#define BASISMATRIXLINEAREQUATIONSYSTEM_H

#include "abstractlinearequationsystem.h"
#include "vector.h"

template< typename NumberType, typename SizeType >
class BasisMatrixLinearEquationSystem : public AbstractLinearEquationSystem< NumberType, SizeType >
{
public:
    BasisMatrixLinearEquationSystem( Matrix< NumberType, SizeType > matrix, std::vector< NumberType > freeCoef ) :
        AbstractLinearEquationSystem< NumberType, SizeType >(matrix, freeCoef) {}

    virtual void solve()
    {
        // mirrors
        Matrix< NumberType, SizeType > &m_matrix = AbstractLinearEquationSystem< NumberType, SizeType >::m_matrix;
        std::vector< NumberType > &m_freeCoef = AbstractLinearEquationSystem< NumberType, SizeType >::m_freeCoef;
        std::vector< NumberType > &m_solution = AbstractLinearEquationSystem< NumberType, SizeType >::m_solution;
        bool &m_bIsConsistent = AbstractLinearEquationSystem< NumberType, SizeType >::m_bIsConsistent;
        bool &m_bVerboseOutput = AbstractLinearEquationSystem< NumberType, SizeType >::m_bVerboseOutput;
        std::ostream *m_verboseOutputStream = AbstractLinearEquationSystem< NumberType, SizeType >::m_verboseOutputStream;

        // initial data
        if (m_bVerboseOutput && m_verboseOutputStream)
        {
            *m_verboseOutputStream << "System: " << std::endl << *this << std::endl;
        }

        Matrix< NumberType, SizeType > alpha(m_matrix);
        Matrix< NumberType, SizeType > inverseBasisMatrix(m_matrix.rowCount(), m_matrix.columnCount());
        std::vector< NumberType > residual(m_matrix.rowCount(), (NumberType)0);
        std::vector< NumberType > solution(m_matrix.columnCount(), (NumberType)1);

        for (SizeType i = 0; i < m_matrix.rowCount(); i++)
        {
            for (SizeType j = 0; j < m_matrix.columnCount(); j++)
            {
                inverseBasisMatrix[j][i] = (NumberType)((i == j) ? 1 : 0);
            }
            residual[i] = m_matrix.row(i) * solution - m_freeCoef[i];
        }

        // initial calculated data
        if (m_bVerboseOutput && m_verboseOutputStream)
        {
            *m_verboseOutputStream << "Initial matrix: " << std::endl << alpha << std::endl;
            *m_verboseOutputStream << "Initial inverse matrix: " << std::endl << inverseBasisMatrix << std::endl;
            *m_verboseOutputStream << "Initial residual: " << std::endl << residual << std::endl;
            *m_verboseOutputStream << "Initial solution: " << std::endl << solution << std::endl;
        }

        Matrix< NumberType, SizeType > nextAlpha(m_matrix);
        Matrix< NumberType, SizeType > nextInverseBasisMatrix(inverseBasisMatrix);
        std::vector< NumberType > nextResidual(residual);
        std::vector< NumberType > nextSolution(solution);

        // iterations
        SizeType k = 0;     // replaced row
        SizeType l = 0;     // inserted row
        for (SizeType iteration = 0; iteration < m_matrix.rowCount(); iteration++)
        {
            for (SizeType i = 0; i < m_matrix.rowCount(); i++)
            {
                for (SizeType j = 0; j < m_matrix.columnCount(); j++)
                {
                    if (j == k)
                    {
                        nextAlpha[j][i] = (alpha[k][i]/alpha[k][l]);
                        nextInverseBasisMatrix[j][i] = (inverseBasisMatrix[k][i]/alpha[k][l]);
                    }
                    else
                    {
                        nextAlpha[j][i] = (alpha[j][i] - (alpha[k][i]/alpha[k][l])*alpha[j][l]);
                        nextInverseBasisMatrix[j][i] = (inverseBasisMatrix[j][i] - (inverseBasisMatrix[k][i]/alpha[k][l])*alpha[j][l]);
                    }
                }
                nextSolution[i] = solution[i] - (inverseBasisMatrix[k][i]/alpha[k][l])*residual[l];

                if (i == k)
                {
                    nextResidual[i] = - (residual[l]/alpha[k][l]);
                    if (i == 0 && m_verboseOutputStream)
                    {
                        *m_verboseOutputStream << "calculated: " << residual[l] << " / " << alpha[k][l] << " = " << nextResidual[i] << std::endl;
                        *m_verboseOutputStream << std::endl;
                    }
                }
                else
                {
                    nextResidual[i] = (residual[i] - (alpha[k][i]/alpha[k][l])*residual[l]);
                }
            }

            alpha = nextAlpha;
            inverseBasisMatrix = nextInverseBasisMatrix;
            solution = nextSolution;
            residual = nextResidual;

            l = findNextWorkingRow(k);
            k = l;

            if (m_bVerboseOutput && m_verboseOutputStream)
            {
                *m_verboseOutputStream << std::endl;
                *m_verboseOutputStream << "Iteration " << iteration << " matrix: " << std::endl << alpha << std::endl;
                *m_verboseOutputStream << "Iteration " << iteration << " inverse matrix: " << std::endl << inverseBasisMatrix << std::endl;
                *m_verboseOutputStream << "Iteration " << iteration << " residual: " << std::endl << residual << std::endl;
                *m_verboseOutputStream << "Iteration " << iteration << " solution: " << std::endl << solution << std::endl;
            }
        }

        m_solution = solution;
        m_residual = residual;

        if (m_bVerboseOutput && m_verboseOutputStream)
        {
            *m_verboseOutputStream << "Solution: " << m_solution << std::endl;
            *m_verboseOutputStream << "Residual: " << m_residual << std::endl;
        }
    }

    SizeType findNextWorkingRow(SizeType currentWorkingRow)
    {
        return currentWorkingRow+1;
//        if (m_matrix[currentWorkingRow][currentWorkingRow] != (NumberType)0)
//        {
//            return m_matrix.row(currentWorkingRow);
//        }
//        else
//        {
//            for (SizeType i = currentWorkingRow + 1; i < m_matrix.columnCount(); i++)
//            {
//                if (m_matrix[currentWorkingRow][i] != (NumberType)0)
//                {
//                    swapRows(currentWorkingRow, i);
//                    return m_matrix.row(currentWorkingRow);
//                }
//            }
//            throw new LinearEquationSystemException(LinearEquationSystemExceptionCode_InconsistentSystem);
//        }
    }

    std::vector< NumberType > residual() const { return m_residual; }

private:
    std::vector< NumberType > m_residual;
};

#endif // BASISMATRIXLINEAREQUATIONSYSTEM_H
