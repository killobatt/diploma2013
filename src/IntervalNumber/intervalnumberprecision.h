#ifndef INTERVALNUMBERPRECISION_H
#define INTERVALNUMBERPRECISION_H

template< typename NumberType >
NumberType delta(NumberType a);

template< typename NumberType >
NumberType deltaPlus(NumberType a, NumberType b);

template< typename NumberType >
NumberType deltaMult(NumberType a, NumberType b);

template< typename NumberType >
NumberType deltaDiv(NumberType a, NumberType b);

static long double ldblMaxDelta = 0.0L;
static long double ldblMaxDeltaPlus = 0.0L;
static long double ldblMaxDeltaMult = 0.0L;
static long double ldblMaxDeltaDiv = 0.0L;

long double delta(long double a);
long double deltaPlus(long double a, long double b);
long double deltaMult(long double a, long double b);
long double deltaDiv(long double a, long double b);


#endif // INTERVALNUMBERPRECISION_H
