#ifndef NARROWINTERVALNUMBER_H
#define NARROWINTERVALNUMBER_H

#include "intervalnumberimplementation.h"

template< typename NumberType >
class NarrowIntervalNumber: public IntervalNumberImplementation< NumberType >
{
public:
    NarrowIntervalNumber()
    {
        m_med = (NumberType)0.0l;
        m_wid = delta(0.0l);
//        std::cout << "wid" << m_wid << std::endl;
    }

    NarrowIntervalNumber(NumberType med)
    {
        m_med = med;
        m_wid = delta(med);
//        std::cout << "wid" << m_wid << std::endl;
    }

    NarrowIntervalNumber(NumberType med, NumberType wid)
    {
        m_med = med;
        m_wid = wid;
//        std::cout << "wid" << m_wid << std::endl;
    }

    ~NarrowIntervalNumber() {}

    virtual IntervalNumberImplementation< NumberType >* add(IntervalNumberImplementation< NumberType > *other)
    {
        return new NarrowIntervalNumber< NumberType >(m_med + other->med(), m_wid + other->wid() + deltaPlus(m_med, other->med()));
    }

    virtual IntervalNumberImplementation< NumberType >* sub(IntervalNumberImplementation< NumberType > *other)
    {
        return new NarrowIntervalNumber< NumberType >(m_med - other->med(), m_wid + other->wid() + deltaPlus(m_med, other->med()));
    }

    virtual IntervalNumberImplementation< NumberType >* mult(IntervalNumberImplementation< NumberType > *other)
    {
        NumberType wid;
        NumberType med;

        med = m_med * other->med();
        wid = m_med * other->wid() + other->med() * other->wid();
        if (wid < 0)
        {
            wid = - wid;
        }

//        if (m_med >= 0 && other->med() >= 0)
//        {
//            med = m_med * other->med() + m_wid * other->wid();
//            wid = m_med * other->wid() + other->med() * other->wid();
//        }
//        else if (m_med >= 0 && other->med() <  0)
//        {
//            med = m_med * other->med() - m_wid * other->wid();
//            wid = m_med * other->wid() - other->med() * other->wid();
//        }
//        else if (m_med <  0 && other->med() >= 0)
//        {
//            med = m_med * other->med() - m_wid * other->wid();
//            wid = - m_med * other->wid() + other->med() * other->wid();
//        }
//        else if (m_med <  0 && other->med() <  0)
//        {
//            med = m_med * other->med() + m_wid * other->wid();
//            wid = - m_med * other->wid() - other->med() * other->wid();
//        }

        wid += deltaMult(m_med, other->med());
        return new NarrowIntervalNumber< NumberType >(med, wid);
    }

    virtual IntervalNumberImplementation< NumberType >* div(IntervalNumberImplementation< NumberType > *other)
    {
        NumberType med;
        NumberType wid;

        med = m_med / other->med();

        NumberType denominator = (other->med() * other->med() - other->wid() * other->wid());
        wid = (m_med * other->wid() + other->med() * other->wid()) / denominator;
        if (wid < 0)
        {
            wid = - wid;
        }
//        if (m_med >= 0 && other->med() >= 0)
//        {
//            med = (m_med * other->med() + m_wid * other->wid()) / denominator;
//            wid = (m_med * other->wid() + other->med() * other->wid()) / denominator;
//        }
//        else if (m_med >= 0 && other->med() <  0)
//        {
//            med = (m_med * other->med() - m_wid * other->wid()) / denominator;
//            wid = (m_med * other->wid() - other->med() * other->wid()) / denominator;
//        }
//        else if (m_med <  0 && other->med() >= 0)
//        {
//            med = (m_med * other->med() - m_wid * other->wid()) / denominator;
//            wid = (- m_med * other->wid() + other->med() * other->wid()) / denominator;
//        }
//        else if (m_med <  0 && other->med() <  0)
//        {
//            med = (m_med * other->med() + m_wid * other->wid()) / denominator;
//            wid = (- m_med * other->wid() - other->med() * other->wid()) / denominator;
//        }

        wid += deltaDiv(m_med, other->med());
        return new NarrowIntervalNumber< NumberType >(med, wid);
    }

    virtual IntervalNumberImplementation< NumberType >* opposite()
    {
        return new NarrowIntervalNumber< NumberType >(- m_med, m_wid);
    }

    virtual bool equals(IntervalNumberImplementation< NumberType > *other)
    {
        return (other->low() <= low() && low() <= other->high()) ||
                (other->low() <= high() && high() <= other->high()) ||
                (low() <= other->low() && other->low() <= high()) ||
                (low() <= other->high() && other->high() <= high());
    }

    virtual void read(std::istream& stream)
    {
        stream >> m_med;
        m_wid = delta(m_med);
    }

    virtual void write(std::ostream& stream)
    {
        stream << "(" << std::setprecision(30) << std::setw(10) << m_med <<
                  " ± " << std::setprecision(30) << std::setw(10) << m_wid << ")";
//        stream << "(" << m_med << " ± " << m_wid << ")";
    }

    virtual NumberType low() const { return m_med - m_wid; }
    virtual NumberType high() const { return m_med + m_wid; }
    virtual NumberType wid() const { return m_wid; }
    virtual NumberType med() const { return m_med; }

private:
    NumberType m_med;
    NumberType m_wid;
};

#endif // NARROWINTERVALNUMBER_H
