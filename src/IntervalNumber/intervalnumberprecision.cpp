#include "intervalnumberprecision.h"

#include "math.h"
#include "float.h"
#include "iostream"
#include "cassert"

#define LDBL_MANT_DIG_CNT LDBL_MANT_DIG
#define  DBL_MANT_DIG_CNT 52
#define  FLT_MANT_DIG_CNT 22

long double delta(long double a)
{
//    return 0;
    int pow = 0;
    frexpl(a, &pow);
    long double result = ldexpl(1, pow - LDBL_MANT_DIG_CNT + 1);
    if (ldblMaxDelta < result) ldblMaxDelta = result;
    return result;
}

long double deltaPlus(long double a, long double b)
{
//    return 0;
    int expA, expB;
    frexpl(a, &expA);
    frexpl(b, &expB);
    long double result;

    if (expA == expB)
    {
        result = 0;
    }
    else
    {
        int maxExp = (expA < expB) ? expB : expA;

        if (abs(expA - expB) < LDBL_MANT_DIG_CNT)
        {
            result = ldexpl(1.0L, maxExp - LDBL_MANT_DIG_CNT + 1);
        }
        else
        {
            result = (maxExp == expA) ? fabsl(b) : fabsl(a);
        }
    }

    if (ldblMaxDeltaPlus < result) ldblMaxDeltaPlus = result;

    if (result < 0.0L)
    {
        result = fabsl(result);
    }

    return result;
}

long double deltaMult(long double a, long double b)
{
//    return 0;
    int expA, expB;
    frexpl(a, &expA);
    frexpl(b, &expB);
    long double result = ldexpl(1, expA + expB - LDBL_MANT_DIG_CNT + 1);
    if (ldblMaxDeltaMult < result) ldblMaxDeltaMult = result;
    return result;
}

long double deltaDiv(long double a, long double b)
{
//    return 0;
    int expA, expB;
    frexpl(a, &expA);
    frexpl(b, &expB);
    long double result = ldexpl(1, expA - expB - LDBL_MANT_DIG_CNT + 1);
    if (ldblMaxDeltaDiv < result) ldblMaxDeltaDiv = result;
    return result;
}
