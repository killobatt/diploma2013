#ifndef INTERVALNUMBER_H
#define INTERVALNUMBER_H

#include "../commons.h"
#include "narrowintervalnumber.h"
#include "wideintervalnumber.h"

template< typename NumberType >
class IntervalNumber
{
public:
    IntervalNumber()
    {
        m_currentImplementation = new NarrowIntervalNumber< NumberType >(0.0f);
    }

    IntervalNumber(int med)
    {
        m_currentImplementation = new NarrowIntervalNumber< NumberType >((NumberType)med);
    }

    IntervalNumber(NumberType med)
    {
        m_currentImplementation = new NarrowIntervalNumber< NumberType >(med);
    }

    IntervalNumber(NumberType low, NumberType high)
    {
        m_currentImplementation = new NarrowIntervalNumber< NumberType >(low, high);
        // wideintnum(low, high);
    }

    IntervalNumber(const IntervalNumber& a)
    {
        if (a.m_currentImplementation == NULL)
        {
            m_currentImplementation = new NarrowIntervalNumber< NumberType >(0.0f);
        }
        else
        {
            m_currentImplementation = getBetterImplementationForImplementation(a.m_currentImplementation);
        }
    }

    IntervalNumber(IntervalNumberImplementation< NumberType > *implementation)
    {
        m_currentImplementation = getBetterImplementationForImplementation(implementation);
    }

    IntervalNumberImplementation< NumberType > * getBetterImplementationForImplementation(IntervalNumberImplementation< NumberType > *implementation)
    {
        if (fabsl(implementation->med()) > fabsl(10 * implementation->wid()))
        {
            return new NarrowIntervalNumber< NumberType >(implementation->med(), implementation->wid());
        }
        else
        {
//            std::cout << "Create wide IN: med = " << implementation->med() << " wid = " << implementation->wid() <<
//                         " low = " << implementation->low() << " high = " << implementation->high() << std::endl;
            return new WideIntervalNumber< NumberType >(implementation->low(), implementation->high());
        }
    }

    ~IntervalNumber()
    {
//        if (m_currentImplementation) delete m_currentImplementation;
    }

//    template< typename NumberType >
//    friend IntervalNumber< NumberType > operator +(IntervalNumber< NumberType > a, IntervalNumber< NumberType > b);

//    template< typename NumberType >
//    friend IntervalNumber< NumberType > operator +(NumberType a, IntervalNumber< NumberType > b)
//    {
//        return IntervalNumber< NumberType >(a + b.low() - deltaPlus(a, b.low()),
//                                            a + b.high() + deltaPlus(a, b.high()));
//    }

//    template< typename NumberType >
//    IntervalNumber< NumberType > operator +(IntervalNumber< NumberType > a, NumberType b)
//    {
//        return IntervalNumber< NumberType >(a.low() + b - deltaPlus(a.low(), b),
//                                            a.high() + b + deltaPlus(a.high(), b));
//    }

//    template< typename NumberType >
//    friend IntervalNumber< NumberType > operator -(IntervalNumber< NumberType > a, IntervalNumber< NumberType > b);

//    template< typename NumberType >
//    IntervalNumber< NumberType > operator -(NumberType a, IntervalNumber< NumberType > b)
//    {
//        return IntervalNumber< NumberType >(a - b.high() - deltaPlus(a, b.high()),
//                                            a - b.low() + deltaPlus(a, b.low()));
//    }

//    template< typename NumberType >
//    IntervalNumber< NumberType > operator -(IntervalNumber< NumberType > a, NumberType b)
//    {
//        return IntervalNumber< NumberType >(a.low() - b - deltaPlus(a.low(), b),
//                                            a.high() - b + deltaPlus(a.high(), b));
//    }

//    template< typename NumberType >
//    friend IntervalNumber< NumberType > operator *(IntervalNumber< NumberType > a, IntervalNumber< NumberType > b);
//    template< typename NumberType >
//    IntervalNumber< NumberType > operator *(NumberType a, IntervalNumber< NumberType > b)
//    {
//        return IntervalNumber< NumberType >(a * b.low() - deltaMult(a, b.low()),
//                                            a * b.high() + deltaMult(a, b.high()));
//    }

//    template< typename NumberType >
//    IntervalNumber< NumberType > operator *(IntervalNumber< NumberType > a, NumberType b)
//    {
//        return IntervalNumber< NumberType >(b * a.low() - deltaMult(b, a.low()),
//                                            b * a.high() + deltaMult(b, a.high()) );
//    }

//    template< typename NumberType >
//    friend IntervalNumber< NumberType > operator /(IntervalNumber< NumberType > a, IntervalNumber< NumberType > b);

//    template< typename NumberType >
//    IntervalNumber< NumberType > operator /(NumberType a, IntervalNumber< NumberType > b)
//    {
//        NumberType one = (NumberType)1.0f;
//        return a * IntervalNumber< NumberType >(one / b.high() - deltaDiv(one, b.high()),
//                                                one / b.low() + deltaDiv(one, b.low()));
//    }

//    template< typename NumberType >
//    IntervalNumber< NumberType > operator /(IntervalNumber< NumberType > a, NumberType b)
//    {
//        return a * (1 / b);
//    }

//    template< typename NumberType >
//    friend IntervalNumber< NumberType > & operator +=(IntervalNumber< NumberType > &a, IntervalNumber< NumberType > b);

//    template< typename NumberType >
//    friend IntervalNumber< NumberType > & operator -=(IntervalNumber< NumberType > &a, IntervalNumber< NumberType > b);

//    template< typename NumberType >
//    friend IntervalNumber< NumberType > & operator *=(IntervalNumber< NumberType > &a, IntervalNumber< NumberType > b);

//    template< typename NumberType >
//    friend IntervalNumber< NumberType > & operator /=(IntervalNumber< NumberType > &a, IntervalNumber< NumberType > b);

//    template< typename NumberType >
//    friend IntervalNumber< NumberType > operator -(IntervalNumber< NumberType > a);

//    template< typename NumberType >
//    friend std::ostream& operator <<(std::ostream& stream, IntervalNumber< NumberType > &a);

//    template< typename NumberType >
//    friend std::istream& operator >>(std::istream& stream, IntervalNumber< NumberType > &a);

//    template< typename NumberType >
//    friend bool operator ==(IntervalNumber< NumberType > a, IntervalNumber< NumberType > b);

//    template< typename NumberType >
//    bool operator ==(NumberType a, IntervalNumber< NumberType > b)
//    {
//        return b.low() <= a && a <= b.high();
//    }

//    template< typename NumberType >
//    bool operator ==(IntervalNumber< NumberType > a, NumberType b)
//    {
//        return b.low() <= a && a <= b.high();
//    }

//    template< typename NumberType >
//    friend bool operator !=(IntervalNumber< NumberType > a, IntervalNumber< NumberType > b);

//    template< typename NumberType >
//    bool operator !=(NumberType a, IntervalNumber< NumberType > b)
//    {
//        return !(a == b);
//    }

//    template< typename NumberType >
//    bool operator !=(IntervalNumber< NumberType > a, NumberType b)
//    {
//        return !(a == b);
//    }

    NumberType low() const { return currentImplementation()->low(); }
    NumberType high() const { return currentImplementation()->high(); }
    NumberType med() const { return currentImplementation()->med(); }
    NumberType wid() const { return currentImplementation()->wid(); }

    IntervalNumberImplementation< NumberType >* currentImplementation() const { return m_currentImplementation; }

    IntervalNumberImplementation< NumberType >* setCurrentImplementation(IntervalNumberImplementation< NumberType > *implementation)
    {
        if (m_currentImplementation != NULL)
            delete m_currentImplementation;
        m_currentImplementation = implementation;
    }

private:

    IntervalNumberImplementation< NumberType >* m_currentImplementation;

};

template< typename NumberType >
IntervalNumber< NumberType > operator +(IntervalNumber< NumberType > a, IntervalNumber< NumberType > b)
{
    IntervalNumberImplementation< NumberType >* impl = a.currentImplementation()->add(b.currentImplementation());
    return IntervalNumber< NumberType >(impl);
}

template< typename NumberType >
IntervalNumber< NumberType > operator -(IntervalNumber< NumberType > a, IntervalNumber< NumberType > b)
{
    IntervalNumberImplementation< NumberType >* impl = a.currentImplementation()->sub(b.currentImplementation());
    return IntervalNumber< NumberType >(impl);
}

template< typename NumberType >
IntervalNumber< NumberType > operator *(IntervalNumber< NumberType > a, IntervalNumber< NumberType > b)
{
    IntervalNumberImplementation< NumberType >* impl = a.currentImplementation()->mult(b.currentImplementation());
    return IntervalNumber< NumberType >(impl);
}

template< typename NumberType >
IntervalNumber< NumberType > operator /(IntervalNumber< NumberType > a, IntervalNumber< NumberType > b)
{
    IntervalNumberImplementation< NumberType >* impl = a.currentImplementation()->div(b.currentImplementation());
    return IntervalNumber< NumberType >(impl);
}

template< typename NumberType >
IntervalNumber< NumberType > & operator +=(IntervalNumber< NumberType > &a, IntervalNumber< NumberType > b)
{
    a = a + b;
    return a;
}

template< typename NumberType >
IntervalNumber< NumberType > & operator -=(IntervalNumber< NumberType > &a, IntervalNumber< NumberType > b)
{
    a = a - b;
    return a;
}

template< typename NumberType >
IntervalNumber< NumberType > & operator *=(IntervalNumber< NumberType > &a, IntervalNumber< NumberType > b)
{
    a = a * b;
    return a;
}

template< typename NumberType >
IntervalNumber< NumberType > & operator /=(IntervalNumber< NumberType > &a, IntervalNumber< NumberType > b)
{
    a = a / b;
    return a;
}

template< typename NumberType >
IntervalNumber< NumberType > operator -(IntervalNumber< NumberType > a)
{
    IntervalNumberImplementation< NumberType >* impl = a.currentImplementation()->opposite();
    return IntervalNumber< NumberType >(impl);
}

template< typename NumberType >
std::ostream& operator <<(std::ostream& stream, IntervalNumber< NumberType > a)
{
    a.currentImplementation()->write(stream);
    return stream;
}

template< typename NumberType >
std::istream& operator >>(std::istream& stream, IntervalNumber< NumberType > &a)
{
    a.currentImplementation()->read(stream);
    return stream;
}

template< typename NumberType >
bool operator ==(IntervalNumber< NumberType > a, IntervalNumber< NumberType > b)
{
    return a.currentImplementation()->equals(b.currentImplementation());
}

template< typename NumberType >
bool operator !=(IntervalNumber< NumberType > a, IntervalNumber< NumberType > b)
{
    return !(a == b);
}

#endif // INTERVALNUMBER_H
