#ifndef INTERVALNUMBERIMPLEMENTATION_H
#define INTERVALNUMBERIMPLEMENTATION_H

#include "../commons.h"
#include "intervalnumberconfig.h"
#include "intervalnumberprecision.h"

template< typename NumberType >
class IntervalNumberImplementation
{
public:

    virtual ~IntervalNumberImplementation() {}

    virtual IntervalNumberImplementation< NumberType >* add(IntervalNumberImplementation< NumberType > *other) = 0;
    virtual IntervalNumberImplementation< NumberType >* sub(IntervalNumberImplementation< NumberType > *other) = 0;
    virtual IntervalNumberImplementation< NumberType >* mult(IntervalNumberImplementation< NumberType > *other) = 0;
    virtual IntervalNumberImplementation< NumberType >* div(IntervalNumberImplementation< NumberType > *other) = 0;

    virtual IntervalNumberImplementation< NumberType >* opposite() = 0;

    virtual bool equals(IntervalNumberImplementation< NumberType > *other) = 0;

    virtual void read(std::istream& stream) = 0;
    virtual void write(std::ostream& stream) = 0;

    virtual NumberType low() const = 0;
    virtual NumberType high() const = 0;
    virtual NumberType wid() const = 0;
    virtual NumberType med() const = 0;
};

#endif // INTERVALNUMBERIMPLEMENTATION_H
