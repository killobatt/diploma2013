#ifndef WIDEINTERVALNUMBER_H
#define WIDEINTERVALNUMBER_H

#include "intervalnumberimplementation.h"

template< typename NumberType >
class WideIntervalNumber : public IntervalNumberImplementation< NumberType >
{
public:
    WideIntervalNumber();

    WideIntervalNumber(NumberType med)
    {
        m_low = med - delta(med);
        m_high = med + delta(med);
    }

    WideIntervalNumber(NumberType low, NumberType high)
    {
        m_low = low - delta(low);
        m_high = high + delta(high);
    }

    virtual IntervalNumberImplementation< NumberType >* add(IntervalNumberImplementation< NumberType > *other)
    {
        return new WideIntervalNumber< NumberType >(m_low + other->low() - deltaPlus(m_low, other->low()),
                                                    m_high + other->high() + deltaPlus(m_high, other->high()));
    }

    virtual IntervalNumberImplementation< NumberType >* sub(IntervalNumberImplementation< NumberType > *other)
    {
        return new WideIntervalNumber< NumberType >(m_low - other->high() - deltaPlus(m_low, other->high()),
                                                    m_high - other->low() + deltaPlus(m_high, other->low()));
    }

    virtual IntervalNumberImplementation< NumberType >* mult(IntervalNumberImplementation< NumberType > *other)
    {
        NumberType result[4] =
        {
            m_low * other->low(),
            m_low * other->high(),
            m_high * other->low(),
            m_high * other->high()
        };

        NumberType delta[4] =
        {
            deltaMult(m_low, other->low()),
            deltaMult(m_low, other->high()),
            deltaMult(m_high, other->low()),
            deltaMult(m_high, other->high())
        };

        NumberType low = result[0];
        NumberType high = result[0];
        NumberType deltaLow = delta[0];
        NumberType deltaHigh = delta[0];

        for (int i = 1; i < result[i]; i++)
        {
            if (result[i] < low)
            {
                low = result[i];
                deltaLow = delta[i];
            }
            if (high < result[i])
            {
                high = result[i];
                deltaHigh = delta[i];
            }
        }

        return new WideIntervalNumber< NumberType >(low - deltaLow, high + deltaHigh);
    }

    virtual IntervalNumberImplementation< NumberType >* div(IntervalNumberImplementation< NumberType > *other)
    {
        NumberType one = (NumberType)1.0f;
        WideIntervalNumber< NumberType > *reverseOther = new WideIntervalNumber< NumberType >(one / other->high() - deltaDiv(one, other->high()),
                                                                                              one / other->low() + deltaDiv(one, other->low()));
        return this->mult(reverseOther);
    }

    virtual IntervalNumberImplementation< NumberType >* opposite()
    {
        return new WideIntervalNumber< NumberType >(- m_high, - m_low);
    }

    virtual bool equals(IntervalNumberImplementation< NumberType > *other)
    {
        return (other->low() <= m_low && m_low <= other->high()) ||
                (other->low() <= m_high && m_high <= other->high()) ||
                (m_low <= other->low() && other->low() <= m_high) ||
                (m_low <= other->high() && other->high() <= m_high);
    }

    virtual void read(std::istream& stream)
    {
        NumberType med;
        stream >> med;
        m_low = med - delta(med);
        m_high = med + delta(med);
    }

    virtual void write(std::ostream& stream)
    {
//        stream << "(" << std::setprecision(5) << std::setw(5) << m_low <<
//                  ", " << std::setprecision(5) << std::setw(5) << m_high << ")" <<
//                  " wid = " << std::setprecision(5) << std::setw(5) << wid();
        stream << "(" << m_low << ", " << m_high << ")";
    }

    virtual NumberType low() const { return m_low; }
    virtual NumberType high() const { return m_high; }
    virtual NumberType wid() const { return (m_high - m_low) * 0.5f; }
    virtual NumberType med() const { return (m_low + m_high) * 0.5f; }

private:
    NumberType m_low;
    NumberType m_high;
};

#endif // WIDEINTERVALNUMBER_H
