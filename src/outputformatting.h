#ifndef GMPEXT_H
#define GMPEXT_H

#include <iostream>

// gmp
#include "gmpxx.h"
std::ostream& operator<<(std::ostream& stream, const mpz_class &mpz);
std::ostream& operator<<(std::ostream& stream, const mpq_class &mpq);
std::ostream& operator<<(std::ostream& stream, const mpf_class &mpf);
std::istream& operator>>(std::istream& stream, mpz_class &mpz);
std::istream& operator>>(std::istream& stream, mpq_class &mpq);
std::istream& operator>>(std::istream& stream, mpf_class &mpf);

// vector
#include <vector>
template< typename T >
std::ostream& operator <<(std::ostream& stream, const std::vector< T > &vector)
{
    for (uint64_t i = 0; i < vector.size(); i++)
    {
        T ith = vector[i];
        stream << ith << " ";
    }
    return stream;
}

#endif // GMPEXT_H
