#ifndef LINEAREQUATIONSYSTEM_H
#define LINEAREQUATIONSYSTEM_H

#include "abstractlinearequationsystem.h"

template <typename NumberType, typename SizeType>
class LinearEquationSystem : public AbstractLinearEquationSystem< NumberType, SizeType >
{

public:
    LinearEquationSystem( Matrix< NumberType, SizeType > matrix, std::vector< NumberType > freeCoef ) :
        AbstractLinearEquationSystem< NumberType, SizeType >(matrix, freeCoef) {}

    virtual void solve()
    {
        // mirrors
        Matrix< NumberType, SizeType > &m_matrix = AbstractLinearEquationSystem< NumberType, SizeType >::m_matrix;
        std::vector< NumberType > &m_freeCoef = AbstractLinearEquationSystem< NumberType, SizeType >::m_freeCoef;
        std::vector< NumberType > &m_solution = AbstractLinearEquationSystem< NumberType, SizeType >::m_solution;
        bool &m_bIsConsistent = AbstractLinearEquationSystem< NumberType, SizeType >::m_bIsConsistent;
        bool &m_bVerboseOutput = AbstractLinearEquationSystem< NumberType, SizeType >::m_bVerboseOutput;
        std::ostream *m_verboseOutputStream = AbstractLinearEquationSystem< NumberType, SizeType >::m_verboseOutputStream;

        if (m_bVerboseOutput)
        {
            *m_verboseOutputStream << *this << std::endl << std::endl;
        }

        // Gauss method
        for (SizeType rowIndex = 0; rowIndex < m_matrix.rowCount(); rowIndex++)
        {
            std::vector< NumberType > row = findNextWorkingRow(rowIndex);
            NumberType alpha = row[rowIndex];           
            NumberType one = (NumberType)1;
            NumberType alphaReverse = one / alpha;
            row = (alphaReverse) * row;
            m_freeCoef[rowIndex] = m_freeCoef[rowIndex] / alpha;

            for (SizeType i = rowIndex + 1; i < m_matrix.rowCount(); i++)
            {
                std::vector< NumberType > nextRow = m_matrix.row(i);
                NumberType beta = nextRow[rowIndex];
                nextRow = nextRow - beta * row;
                NumberType m_freeCoefRowIndex = m_freeCoef[rowIndex];
                m_freeCoefRowIndex = m_freeCoefRowIndex * beta;
                m_freeCoef[i] = m_freeCoef[i] - m_freeCoefRowIndex;
                m_matrix.setRow( nextRow, i );
            }
            m_matrix.setRow( row, rowIndex );

            if (m_bVerboseOutput)
            {
                *m_verboseOutputStream << *this << std::endl << std::endl;
            }
        }
        m_solution = gatherSolution();
    }

    std::vector< NumberType > gatherSolution()
    {
        // mirrors
        Matrix< NumberType, SizeType > &m_matrix = AbstractLinearEquationSystem< NumberType, SizeType >::m_matrix;
        std::vector< NumberType > &m_freeCoef = AbstractLinearEquationSystem< NumberType, SizeType >::m_freeCoef;
        std::vector< NumberType > &m_solution = AbstractLinearEquationSystem< NumberType, SizeType >::m_solution;
        bool &m_bIsConsistent = AbstractLinearEquationSystem< NumberType, SizeType >::m_bIsConsistent;
        bool &m_bVerboseOutput = AbstractLinearEquationSystem< NumberType, SizeType >::m_bVerboseOutput;

        std::vector< NumberType > solution(m_matrix.columnCount(), (NumberType)0);
        for (SizeType i = m_matrix.columnCount()-1; ; i--)
        {
            NumberType summ = m_freeCoef[i];
            for (SizeType j = i+1; j < m_matrix.rowCount(); j++)
            {
                summ -= m_matrix[j][i] * solution[j];
            }
            solution[i] = summ;
            if (i == 0) break;  // instead of for (SizeType i = ...; i >= 0; i--) : SizeType can be unsigned.
        }
        return solution;
    }

    std::vector< NumberType > findNextWorkingRow(SizeType currentWorkingRow)
    {
        // mirrors
        Matrix< NumberType, SizeType > &m_matrix = AbstractLinearEquationSystem< NumberType, SizeType >::m_matrix;
        std::vector< NumberType > &m_freeCoef = AbstractLinearEquationSystem< NumberType, SizeType >::m_freeCoef;
        std::vector< NumberType > &m_solution = AbstractLinearEquationSystem< NumberType, SizeType >::m_solution;
        bool &m_bIsConsistent = AbstractLinearEquationSystem< NumberType, SizeType >::m_bIsConsistent;
        bool &m_bVerboseOutput = AbstractLinearEquationSystem< NumberType, SizeType >::m_bVerboseOutput;

        NumberType curElement = m_matrix[currentWorkingRow][currentWorkingRow];
        if (curElement != (NumberType)0)
        {
            return m_matrix.row(currentWorkingRow);
        }
        else
        {
            for (SizeType i = currentWorkingRow + 1; i < m_matrix.columnCount(); i++)
            {
                if (m_matrix[currentWorkingRow][i] != (NumberType)0)
                {
                    swapRows(currentWorkingRow, i);
                    return m_matrix.row(currentWorkingRow);
                }
            }
//            throw new LinearEquationSystemException(LinearEquationSystemExceptionCode_InconsistentSystem);
        }
    }

    void swapRows(SizeType i, SizeType j)
    {
        // mirrors
        Matrix< NumberType, SizeType > &m_matrix = AbstractLinearEquationSystem< NumberType, SizeType >::m_matrix;
        std::vector< NumberType > &m_freeCoef = AbstractLinearEquationSystem< NumberType, SizeType >::m_freeCoef;
        std::vector< NumberType > &m_solution = AbstractLinearEquationSystem< NumberType, SizeType >::m_solution;
        bool &m_bIsConsistent = AbstractLinearEquationSystem< NumberType, SizeType >::m_bIsConsistent;
        bool &m_bVerboseOutput = AbstractLinearEquationSystem< NumberType, SizeType >::m_bVerboseOutput;

        std::vector< NumberType > rowI = m_matrix.row(i);
        std::vector< NumberType > rowJ = m_matrix.row(j);
        m_matrix.setRow(rowI, j);
        m_matrix.setRow(rowJ, i);
    }

};

#endif // LINEAREQUATIONSYSTEM_H
