#ifndef VECTOR_H
#define VECTOR_H

#include "commons.h"

class VectorException: public std::exception
{
};

template< typename T >
std::vector< T > operator +(const std::vector< T > &a, const std::vector< T > &b)
{
    if (a.size() != b.size())
    {
        throw new VectorException();
    }
    std::vector< T > result(a.size(), (T)0);
    for (uint64_t i = 0; i < a.size(); i++)
    {
        result[i] = a[i] + b[i];
    }
    return result;
}

template< typename T >
std::vector< T > operator -(const std::vector< T > &a, const std::vector< T > &b)
{
    if (a.size() != b.size())
    {
        throw new VectorException();
    }
    std::vector< T > result(a.size(), (T)0);
    for (uint64_t i = 0; i < a.size(); i++)
    {
        result[i] = a[i] - b[i];
    }
    return result;
}

template< typename T >
T operator *(const std::vector< T > &a, const std::vector< T > &b)
{
    if (a.size() != b.size())
    {
        throw new VectorException();
    }
    T result = (T)0;
    for (uint64_t i = 0; i < a.size(); i++)
    {
        result += a[i] * b[i];
    }
    return result;
}

template< typename T >
std::vector< T > operator *(T alpha, const std::vector< T > &a)
{
    std::vector< T > result(a.size(), (T)0);
    for (uint64_t i = 0; i < a.size(); i++)
    {
        result[i] = alpha * a[i];
    }
    return result;
}

template< typename T >
void operator *=(std::vector< T > &a, T alpha)
{
    for (uint64_t i = 0; i < a.size(); i++)
    {
        a[i] = a[i] * alpha;
    }
}

#endif // VECTOR_H
