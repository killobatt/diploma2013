#include "outputformatting.h"

std::ostream& operator<<(std::ostream& stream, const mpz_class &mpz)
{
    stream << mpz.get_str();
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const mpq_class &mpq)
{
    stream << mpq.get_str();
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const mpf_class &mpf)
{
    mp_exp_t exp = 10;
    stream << mpf.get_str(exp, 10);
    return stream;
}

std::istream& operator>>(std::istream& stream, mpz_class &mpz)
{
    char symbol;
    std::string string;
    while (!stream.eof())
    {
        symbol = stream.get();
        if (string.size() == 0 && (symbol == '+' || symbol == '-'))
        {
            string.push_back(symbol);
        }
        else if ('0' <= symbol && symbol <= '9')
        {
            string.push_back(symbol);
        }
        else
        {
            stream.unget();
            break;
        }
    }
    mpz = mpz_class(string);
    return stream;
}

std::istream& operator>>(std::istream& stream, mpq_class &mpq)
{
    char symbol;
    std::string string;
    bool seenDivisor = false;
    while (!stream.eof())
    {
        symbol = stream.get();
        if (symbol == '+' || symbol == '-' ||
            ('0' <= symbol && symbol <= '9') ||
            symbol == '/')
        {
            stream.unget();
            break;
        }
    }

    while (!stream.eof())
    {
        symbol = stream.get();
        if (string.size() == 0 && (symbol == '+' || symbol == '-'))
        {
            string.push_back(symbol);
        }
        else if (seenDivisor == false && symbol == '/')
        {
            seenDivisor = true;
            string.push_back(symbol);
        }
        else if ('0' <= symbol && symbol <= '9')
        {
            string.push_back(symbol);
        }
        else
        {
            stream.unget();
            break;
        }
    }
    mpq = mpq_class(string);
    return stream;
}

std::istream& operator>>(std::istream& stream, mpf_class &mpf)
{
    char symbol;
    std::string string;
    bool seenDivisor = false;
    while (!stream.eof())
    {
        symbol = stream.get();
        if (string.size() == 0 && (symbol == '+' || symbol == '-'))
        {
            string.push_back(symbol);
        }
        else if (seenDivisor == false && symbol == '.')
        {
            seenDivisor = true;
            string.push_back(symbol);
        }
        else if ('0' <= symbol && symbol <= '9')
        {
            string.push_back(symbol);
        }
        else
        {
            stream.unget();
            break;
        }
    }
    mpf = mpf_class(string);
    return stream;
}
