#ifndef ABSTRACTLINEAREQUEATIONSYSTEM_H
#define ABSTRACTLINEAREQUEATIONSYSTEM_H

#include "matrix.h"
#include "vector.h"
#include "commons.h"

//typedef enum
//{
//    LinearEquationSystemExceptionCode_InconsistentSystem,
//    LinearEquationSystemExceptionCode_NonSquareMatrix,
//    LinearEquationSystemExceptionCode_MatrixAndFreeCoefSizesMismatch
//} LinearEquationSystemExceptionCode;

//class LinearEquationSystemException: public std::exception
//{
//public:
//    LinearEquationSystemException(LinearEquationSystemExceptionCode code) { m_code = code; }
//    virtual const char* what() const throw();
//    LinearEquationSystemExceptionCode code() const { return m_code; }
//private:
//    LinearEquationSystemExceptionCode m_code;
//};

template <typename NumberType, typename SizeType>
class AbstractLinearEquationSystem
{
public:
    AbstractLinearEquationSystem( Matrix< NumberType, SizeType > matrix, std::vector< NumberType > freeCoef ) :
        m_matrix(matrix),
        m_freeCoef(freeCoef)
    {
        m_bVerboseOutput = false;
        m_verboseOutputStream = NULL;
    }

    virtual void solve() = 0;

    virtual Matrix< NumberType, SizeType > matrix() const { return m_matrix; }
    virtual std::vector< NumberType > freeCoef() const { return m_freeCoef; }
    virtual std::vector< NumberType > solution() const { return m_solution; }
    virtual bool isConsistent() const { return m_bIsConsistent; }

    virtual bool verboseOutput() const { return m_bVerboseOutput; }
    virtual void setVerboseOutput(bool value) { m_bVerboseOutput = value; }
    virtual std::ostream* verboseOutputStream() const { return m_verboseOutputStream; }
    virtual void setVerboseOutputStream(std::ostream *value) { m_verboseOutputStream = value; }

protected:
    Matrix< NumberType, SizeType > m_matrix;
    std::vector< NumberType > m_freeCoef;
    std::vector< NumberType > m_solution;
    bool m_bIsConsistent;
    bool m_bVerboseOutput;
    std::ostream *m_verboseOutputStream;
};

template< typename NmbrType, typename SzType >
std::ostream& operator <<(std::ostream& stream, AbstractLinearEquationSystem< NmbrType, SzType > &system)
{
    for (SzType i = 0; i < system.matrix().rowCount(); i++)
    {
        for (SzType j = 0; j < system.matrix().columnCount(); j++)
        {
            stream << std::setprecision(6) << std::setw(10) << system.matrix()[j][i] << "\t";
        }
        stream << "|\t" << std::setprecision(6) << std::setw(10) << system.freeCoef()[i] << std::endl;
    }
    return stream;
}

#endif // ABSTRACTLINEAREQUEATIONSYSTEM_H
