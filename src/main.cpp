#include <QCoreApplication>

#include <iostream>

#include "vector.h"
#include "matrix.h"
#include "linearequationsystem.h"
#include "IntervalNumber/intervalnumber.h"
#include "IntervalNumber/narrowintervalnumber.h"

int main(int argc, char *argv[])
{
    std::cout << "SLAR 0.1" << std::endl;

    mpq_class mpq = 15;
    mpq = 1.0f / mpq;
    std::cout << mpq << std::endl;

    uint64_t systemSize = 3;

    Matrix< mpq_class, uint64_t > matrix(systemSize, systemSize);
    for (uint32_t i = 0; i < matrix.columnCount(); i++)
    {
        for (uint32_t j = 0; j < matrix.rowCount(); j++)
        {
            mpq_class mpq = i + j + 1;
            matrix[i][j] = 1.0f / mpq;
        }
    }
    std::vector< mpq_class > freeCoef(matrix.rowCount(), (mpq_class)1.0f);

    LinearEquationSystem< mpq_class, uint64_t > linearSystem(matrix, freeCoef);
    std::cout << linearSystem << std::endl;
    linearSystem.solve();
    std::cout << linearSystem.solution() << std::endl;

    std::cout << "Happy end." << std::endl;
//    mpq_class b, c;
//    b = 1;
//    c = "999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999/3";
//    std::cout << "Created numbers" << std::endl;

//    mpq_class d = b + c;
//    mpq_canonicalize(d.__get_mp());
//    std::cout << d.get_str() << std::endl;

    return 0;
}
