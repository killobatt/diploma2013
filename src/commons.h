#ifndef COMMONS_H
#define COMMONS_H

#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <stdint.h>

#include "gmpxx.h"
#include "outputformatting.h"
#include <exception>

#endif //COMMONS_H

