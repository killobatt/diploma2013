#ifndef MATRIX_H
#define MATRIX_H

#include "commons.h"

template< typename NumberType, typename SizeType >
class Matrix
{
public:
    Matrix(SizeType rows, SizeType cols)
    {
        m_cols = std::vector< std::vector< NumberType > >(cols, std::vector< NumberType >(rows, NumberType(0)));
    }

    SizeType columnCount() const
    {
        return (SizeType)m_cols.size();
    }

    SizeType rowCount() const
    {
        return (m_cols.size() > 0) ? m_cols[0].size() : 0;
    }

    std::vector< NumberType >& operator [](SizeType index)
    {
        return m_cols[index];
    }

    std::vector< NumberType > column(SizeType index) const
    {
        return this->operator [](index);
    }

    void setColumn(std::vector< NumberType> column, SizeType index)
    {
        m_cols[index] = column;
    }

    std::vector< NumberType > row(SizeType index) const
    {
        std::vector< NumberType > result(rowCount());
        for (SizeType i = 0; i < result.size(); i++)
        {
            result[i] = m_cols[i][index];
        }
        return result;
    }

    void setRow(std::vector< NumberType> row, SizeType index)
    {
        for (SizeType i = 0; i < columnCount(); i++)
        {
            m_cols[i][index] = row[i];
        }
    }

//    // Fronebius norm
//    NumberType norm() const
//    {
//        NumberType norm = (NumberType)0;
//        for (SizeType i = 0; i < columnCount(); i++)
//        {
//            for (SizeType j = 0; j < rowCount(); j++)
//            {
//                NumberType aij = column(i)[j];
//                norm += (aij * aij);
//            }
//        }
//        norm = sqrt(norm);
//        return norm;
//    }

private:
    std::vector< std::vector< NumberType > > m_cols;
};

template< typename NmbrType, typename SzType >
std::ostream& operator <<(std::ostream& stream, Matrix< NmbrType, SzType > &matrix)
{
    for (SzType i = 0; i < matrix.rowCount(); i++)
    {
        for (SzType j = 0; j < matrix.columnCount(); j++)
        {
            stream << matrix[j][i] << "\t";
        }
        stream << std::endl;
    }
    return stream;
}

#endif // MATRIX_H
