#-------------------------------------------------
#
# Project created by QtCreator 2013-04-24T00:28:16
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = FloatMBMTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += FloatMBMTest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

SOURCES += \
    ../../src/matrix.cpp \
    ../../src/outputformatting.cpp \
    ../../src/vector.cpp \
    ../../src/linearequationsystem.cpp

INCLUDEPATH += ../../src/ \
               ../../include/

win32:LIBS += $$_PRO_FILE_PWD_/lib/win32/libgmp.a
unix:LIBS += $$_PRO_FILE_PWD_/../../lib/osx/libgmp.a

