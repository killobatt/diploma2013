#-------------------------------------------------
#
# Project created by QtCreator 2013-05-03T12:43:31
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = intervalmbmtest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += intervalmbmtest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

SOURCES += \
    ../../src/matrix.cpp \
    ../../src/outputformatting.cpp \
    ../../src/vector.cpp \
    ../../src/linearequationsystem.cpp \
    ../../src/IntervalNumber/intervalnumber.cpp \
    ../../src/IntervalNumber/intervalnumberconfig.cpp \
    ../../src/IntervalNumber/intervalnumberprecision.cpp  \
    ../../src/IntervalNumber/wideintervalnumber.cpp \
    ../../src/IntervalNumber/narrowintervalnumber.cpp

INCLUDEPATH += ../../src/ \
               ../../include/

win32:LIBS += $$_PRO_FILE_PWD_/lib/win32/libgmp.a
unix:LIBS += $$_PRO_FILE_PWD_/../../lib/osx/libgmp.a

#QMAKE_CXXFLAGS += -m128bit-long-double
