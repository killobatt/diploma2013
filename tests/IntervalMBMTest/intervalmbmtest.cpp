#include <QString>
#include <QtTest>

#pragma GCC diagnostic ignored "-Wunused-parameter"

#include "IntervalNumber/intervalnumber.h"
#include "BasisMatrixLinearEquationSystem.h"

class IntervalMBMTest : public QObject
{
    Q_OBJECT
public:
    IntervalMBMTest();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void testCase1();
    void testCase2();
    void testCase3();
    void testCase4();
    void testCase5();

private:
    template< typename NumberType, typename SizeType >
    BasisMatrixLinearEquationSystem< NumberType, SizeType > readSystemFromFile(std::string fileName)
    {
        std::ifstream file(fileName.c_str());

        if (file.is_open())
        {
            std::cout << "Reading file: " << fileName << std::endl;
            uint64_t systemSize;
            file >> systemSize;
            std::cout << "System size: " << systemSize << std::endl;
            Matrix< NumberType, SizeType > matrix(systemSize, systemSize);
            for (SizeType i = 0; i < matrix.rowCount(); i++)
            {
                for (SizeType j = 0; j < matrix.columnCount(); j++)
                {
                    file >> matrix[j][i];
                }
            }

            std::vector< NumberType > freeCoef(systemSize, NumberType(0));
            for (SizeType i = 0; i < freeCoef.size(); i++)
            {
                file >> freeCoef[i];
            }
            return BasisMatrixLinearEquationSystem< NumberType, SizeType >(matrix, freeCoef);
        }
        else
        {
            std::cerr << "Error reading file " << fileName.c_str() << std::endl;
        }
        return BasisMatrixLinearEquationSystem< NumberType, SizeType >(Matrix< NumberType, SizeType >(0,0), std::vector< NumberType >(0));
    }

    template< typename NumberType, typename SizeType >
    std::vector< NumberType > readSolutionFromFile(std::string fileName)
    {
        std::ifstream file(fileName.c_str());

        if (file.is_open())
        {
            uint64_t systemSize;
            file >> systemSize;
            std::vector< NumberType > solution(systemSize, NumberType(0));
            for (SizeType i = 0; i < solution.size(); i++)
            {
                file >> solution[i];
            }
            return solution;
        }
        else
        {
            std::cerr << "Error reading file " << fileName.c_str() << std::endl;
        }
        return std::vector< NumberType >(0);
    }

    template< typename NumberType >
    void writeSolutionToFile(std::vector< NumberType > solution, std::string fileName)
    {
        std::ofstream file(fileName.c_str());

        if (file.is_open())
        {
            for (size_t i = 0; i < solution.size(); i++)
            {
                file << solution[ i ] << std::endl;
            }
            file.close();
        }
    }

};

IntervalMBMTest::IntervalMBMTest()
{
}

void IntervalMBMTest::initTestCase()
{
    IntervalNumber< long double > n(10.0L, 10.1L);
    IntervalNumber< long double > p(10.0L, 10.1L);
    std::cout << "test number: " << (n / p) << std::endl;
}

void IntervalMBMTest::cleanupTestCase()
{
}

void IntervalMBMTest::testCase1()
{
    long double d = 123456.789f;
    int exp = 0;
    std::cout << d << std::endl;
    std::cout << frexpl(d, &exp) << std::endl;
    std::cout << exp << std::endl;

    std::cout << "Delta: " << delta(d) << std::endl;

//    int targetExp = 3;
//    long double helper = 1.0f;
//    long double mask = 0.0f;
//    for (int i = 0; i < targetExp; i++)
//    {
//        mask |= helper;
//        helper << 1;
//    }

//    d &= mask;
//    std::cout << d << std::endl;


    BasisMatrixLinearEquationSystem< IntervalNumber< long double >, uint64_t > linearSystem = readSystemFromFile< IntervalNumber< long double >, uint64_t>("../../input/input01.in");
    std::vector< IntervalNumber< long double > > solution = readSolutionFromFile< IntervalNumber< long double >, uint64_t>("../../output/output01.out");
    std::cout << "System: " << std::endl << linearSystem << std::endl;
    linearSystem.setVerboseOutput(true);
    linearSystem.setVerboseOutputStream(&std::cout);
    linearSystem.solve();
    std::cout << "System: " << std::endl << linearSystem << std::endl;
    std::cout << "Actual solution" << std::endl << linearSystem.solution() << std::endl;
    std::cout << "Expected solution" << std::endl << solution << std::endl;
    QCOMPARE(linearSystem.solution(), solution);
}

void IntervalMBMTest::testCase2()
{
    BasisMatrixLinearEquationSystem< IntervalNumber< long double >, uint64_t > linearSystem = readSystemFromFile< IntervalNumber< long double >, uint64_t>("../../input/input02.in");
    std::vector< IntervalNumber< long double > > solution = readSolutionFromFile< IntervalNumber< long double >, uint64_t>("../../output/output02.out");
    std::cout << "System: " << std::endl << linearSystem << std::endl;
    linearSystem.setVerboseOutput(false);
    linearSystem.solve();
    std::cout << "Solved system: " << std::endl << linearSystem << std::endl;
    std::cout << "Actual solution" << std::endl << linearSystem.solution() << std::endl;
    std::cout << "Expected solution" << std::endl << solution << std::endl;
    QCOMPARE(linearSystem.solution(), solution);
}

void IntervalMBMTest::testCase3()
{
//    try
//    {
//        BasisMatrixLinearEquationSystem< IntervalNumber< long double >, uint64_t > linearSystem = readSystemFromFile< IntervalNumber< long double >, uint64_t>("../../input/input03.in");
//        std::cout << "System: " << std::endl << linearSystem << std::endl;
//        linearSystem.setVerboseOutput(false);
//        linearSystem.solve();
//        std::cout << "System: " << std::endl << linearSystem << std::endl;
//    }
//    catch (BasisMatrixLinearEquationSystemException *exception)
//    {
//        std::cout << "Catched error: " << exception->what() << std::endl;
//        QCOMPARE(exception->code(), BasisMatrixLinearEquationSystemExceptionCode_InconsistentSystem);
//    }
}

void IntervalMBMTest::testCase4()
{
    uint64_t systemSize = 15;

    Matrix< IntervalNumber< long double >, uint64_t > matrix(systemSize, systemSize);
    for (uint32_t i = 0; i < matrix.columnCount(); i++)
    {
        for (uint32_t j = 0; j < matrix.rowCount(); j++)
        {
            long double number = 1.0f / (long double)(i + j + 1);
//            std::cout << "delta: " << deltaDiv(1.0f, (long double)(i + j + 1));
//            #warning DO NOT CALL this constructor from test, call one with single arg
            IntervalNumber< long double > mpq(number); //, deltaDiv(1.0f, (long double)(i + j + 1))
//            std::cout << std::setprecision(320) << mpq.med() << "\t" << mpq.med() + mpq.wid() << "\t" << mpq.wid() << std::endl;
            matrix[i][j] = mpq;
        }
    }
    std::vector< IntervalNumber< long double > > freeCoef(matrix.rowCount(), IntervalNumber< long double >(1));

    std::ofstream outputStream("../../output/IntervalMBMTest/output04_1.out");

    QElapsedTimer timer;
    BasisMatrixLinearEquationSystem< IntervalNumber< long double >, uint64_t > linearSystem(matrix, freeCoef);
    std::cout << "Soving system with size: " << systemSize << std::endl; // << " norm: " << linearSystem.matrix().norm();
    linearSystem.setVerboseOutput(false);
    linearSystem.setVerboseOutputStream(&outputStream);
    timer.start();
    linearSystem.solve();
    std::cout << "Solved system in: " << timer.elapsed() << " ms" << std::endl;
    outputStream.close();

    writeSolutionToFile(linearSystem.solution(), "../../output/IntervalMBMTest/output04.out");

    std::cout << "Max delta:      " << std::setprecision(30) << std::setw(10) << ldblMaxDelta << std::endl;
    std::cout << "Max delta plus: " << std::setprecision(30) << std::setw(10) << ldblMaxDeltaPlus << std::endl;
    std::cout << "Max delta mult: " << std::setprecision(30) << std::setw(10) << ldblMaxDeltaMult << std::endl;
    std::cout << "Max delta div : " << std::setprecision(30) << std::setw(10) << ldblMaxDeltaDiv << std::endl;
    std::cout << "Max long double: " << LDBL_MAX << std::endl;
}

void IntervalMBMTest::testCase5()
{
//    Matrix< IntervalNumber< long double >, uint64_t > matrix(systemSize, systemSize);
//    for (uint32_t i = 0; i < matrix.columnCount(); i++)
//    {
//        for (uint32_t j = 0; j < matrix.rowCount(); j++)
//        {
//            long double number = 1.0f / (long double)(i + j + 1);
////            std::cout << "delta: " << deltaDiv(1.0f, (long double)(i + j + 1));
//            IntervalNumber< long double > mpq(number, deltaDiv(1.0L, (long double)(i + j + 1)));
////            std::cout << std::setprecision(320) << mpq.med() << "\t" << mpq.med() + mpq.wid() << "\t" << mpq.wid() << std::endl;
//            matrix[i][j] = mpq;
//        }
//    }
//    std::vector< IntervalNumber< long double > > freeCoef(matrix.rowCount(), IntervalNumber< long double >(1));

//    std::vector< NumberType > residual(matrix.rowCount(), (NumberType)0);
//    std::vector< NumberType > solution(matrix.columnCount(), (NumberType)1);

//    for (uint32_t i = 0; i < m_matrix.rowCount(); i++)
//    {
//        residual[i] = m_matrix.row(i) * solution - freeCoef[i];
//    }

//    std::cout << (a / b);
}

QTEST_APPLESS_MAIN(IntervalMBMTest)

#include "intervalmbmtest.moc"
