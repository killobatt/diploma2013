#-------------------------------------------------
#
# Project created by QtCreator 2013-04-15T21:33:30
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = linearequationsystemtest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../../src/ \
               ../../include/

SOURCES += linearequationsystemtest.cpp \
    ../../src/matrix.cpp \
    ../../src/outputformatting.cpp \
    ../../src/vector.cpp \
    ../../src/linearequationsystem.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"

win32:LIBS += $$_PRO_FILE_PWD_/lib/win32/libgmp.a
unix:LIBS += $$_PRO_FILE_PWD_/../../lib/osx/libgmp.a
