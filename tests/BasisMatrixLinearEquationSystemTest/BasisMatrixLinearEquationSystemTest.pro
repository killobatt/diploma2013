#-------------------------------------------------
#
# Project created by QtCreator 2013-04-17T23:49:16
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = basismatrixlinearequationsystemtest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += basismatrixlinearequationsystemtest.cpp \
    ../../src/matrix.cpp \
    ../../src/outputformatting.cpp \
    ../../src/vector.cpp \
    ../../src/linearequationsystem.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"

INCLUDEPATH += ../../src/ \
               ../../include/

win32:LIBS += $$_PRO_FILE_PWD_/lib/win32/libgmp.a
unix:LIBS += $$_PRO_FILE_PWD_/../../lib/osx/libgmp.a
