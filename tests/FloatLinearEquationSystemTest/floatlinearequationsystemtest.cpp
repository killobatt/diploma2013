#include <QString>
#include <QtTest>

#include <QString>
#include <QtTest>

#include "linearequationsystem.h"
#include <numeric>


class FloatLinearEquationSystemTest : public QObject
{
    Q_OBJECT    
public:
    FloatLinearEquationSystemTest();

private Q_SLOTS:
    void testCase1();
    void testCase2();
    void testCase3();
    void testCase4();

private:

    template< typename NumberType, typename SizeType >
    LinearEquationSystem< NumberType, SizeType > readSystemFromFile(std::string fileName)
    {
        std::ifstream file(fileName.c_str());

        if (file.is_open())
        {
            std::cout << "Reading file: " << fileName << std::endl;
            uint64_t systemSize;
            file >> systemSize;
            std::cout << "System size: " << systemSize << std::endl;
            Matrix< NumberType, SizeType > matrix(systemSize, systemSize);
            for (SizeType i = 0; i < matrix.rowCount(); i++)
            {
                for (SizeType j = 0; j < matrix.columnCount(); j++)
                {
                    file >> matrix[j][i];
                }
            }

            std::vector< NumberType > freeCoef(systemSize, 0);
            for (SizeType i = 0; i < freeCoef.size(); i++)
            {
                file >> freeCoef[i];
            }
            return LinearEquationSystem< NumberType, SizeType >(matrix, freeCoef);
        }
        else
        {
            std::cerr << "Error reading file " << fileName.c_str() << std::endl;
        }
        return LinearEquationSystem< NumberType, SizeType >(Matrix< NumberType, SizeType >(0,0), std::vector< NumberType >(0));
    }

    template< typename NumberType, typename SizeType >
    std::vector< NumberType > readSolutionFromFile(std::string fileName)
    {
        std::ifstream file(fileName.c_str());

        if (file.is_open())
        {
            uint64_t systemSize;
            file >> systemSize;
            std::vector< NumberType > solution(systemSize, 0);
            for (SizeType i = 0; i < solution.size(); i++)
            {
                file >> solution[i];
            }
            return solution;
        }
        else
        {
            std::cerr << "Error reading file " << fileName.c_str() << std::endl;
        }
        return std::vector< NumberType >(0);
    }

    template< typename NumberType >
    void writeSolutionToFile(std::vector< NumberType > solution, std::string fileName)
    {
        std::ofstream file(fileName.c_str());

        if (file.is_open())
        {
            for (size_t i = 0; i < solution.size(); i++)
            {
                file << std::setprecision(50) << solution[ i ] << std::endl;
            }
            file.close();
        }
    }
};

FloatLinearEquationSystemTest::FloatLinearEquationSystemTest()
{
}

void FloatLinearEquationSystemTest::testCase1()
{
    LinearEquationSystem< long double, uint64_t > linearSystem = readSystemFromFile< long double, uint64_t>("../../input/input01.in");
    std::vector< long double > solution = readSolutionFromFile< long double, uint64_t>("../../output/output01.out");
    std::cout << "System: " << std::endl << linearSystem << std::endl;
    linearSystem.setVerboseOutput(false);
    linearSystem.solve();
    std::cout << "System: " << std::endl << linearSystem << std::endl;
    std::cout << "Actual solution" << std::endl << linearSystem.solution() << std::endl;
    std::cout << "Expected solution" << std::endl << solution << std::endl;
    QCOMPARE(linearSystem.solution(), solution);
}

void FloatLinearEquationSystemTest::testCase2()
{
    LinearEquationSystem< long double, uint64_t > linearSystem = readSystemFromFile< long double, uint64_t>("../../input/input02.in");
    std::vector< long double > solution = readSolutionFromFile< long double, uint64_t>("../../output/output02.out");
    std::cout << "System: " << std::endl << linearSystem << std::endl;
    linearSystem.setVerboseOutput(false);
    linearSystem.solve();
    std::cout << "Solved system: " << std::endl << linearSystem << std::endl;
    std::cout << "Actual solution" << std::endl << linearSystem.solution() << std::endl;
    std::cout << "Expected solution" << std::endl << solution << std::endl;
    QCOMPARE(linearSystem.solution(), solution);
}

void FloatLinearEquationSystemTest::testCase3()
{
//    try
//    {
//        LinearEquationSystem< long double, uint64_t > linearSystem = readSystemFromFile< long double, uint64_t>("../../input/input03.in");
//        std::cout << "System: " << std::endl << linearSystem << std::endl;
//        linearSystem.setVerboseOutput(false);
//        linearSystem.solve();
//            std::cout << "System: " << std::endl << linearSystem << std::endl;
//    }
//    catch (LinearEquationSystemException *exception)
//    {
//        std::cout << "Catched error: " << exception->what() << std::endl;
//        QCOMPARE(exception->code(), LinearEquationSystemExceptionCode_InconsistentSystem);
//    }
}

void FloatLinearEquationSystemTest::testCase4()
{
    uint64_t systemSize = 100;

    Matrix< long double, uint64_t > matrix(systemSize, systemSize);
    for (uint32_t i = 0; i < matrix.columnCount(); i++)
    {
        for (uint32_t j = 0; j < matrix.rowCount(); j++)
        {
            long double mpq = i + j + 1;
            matrix[i][j] = 1.0f / mpq;
//            std::cout << std::setprecision(308) << 1.0f / mpq << std::endl;
        }
    }
    std::vector< long double > freeCoef(matrix.rowCount(), (long double)1.0f);

//    std::cout <<  << 1.123f;

    std::ofstream outputStream("../../output/FloatLinearEquationSystemTest/output04_1.out");

    QElapsedTimer timer;
    LinearEquationSystem< long double, uint64_t > linearSystem(matrix, freeCoef);
    std::cout << "Soving system with size: " << systemSize << std::endl; // << " norm: " << linearSystem.matrix().norm();
    linearSystem.setVerboseOutputStream(&outputStream);
    linearSystem.setVerboseOutput(false);
    timer.start();
    linearSystem.solve();
    std::cout << "Solved system in: " << timer.elapsed() << " ms" << std::endl;
    outputStream << "Solution: " << std::endl << linearSystem.solution() << std::endl;
    outputStream.close();

    writeSolutionToFile(linearSystem.solution(), "../../output/FloatLinearEquationSystemTest/output04.out");
}

QTEST_APPLESS_MAIN(FloatLinearEquationSystemTest)

#include "FloatLinearEquationSystemTest.moc"
