#-------------------------------------------------
#
# Project created by QtCreator 2013-05-01T12:57:06
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = floatlinearequationsystemtest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += floatlinearequationsystemtest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

SOURCES += \
    ../../src/matrix.cpp \
    ../../src/outputformatting.cpp \
    ../../src/vector.cpp \
    ../../src/linearequationsystem.cpp

INCLUDEPATH += ../../src/ \
               ../../include/

win32:LIBS += $$_PRO_FILE_PWD_/lib/win32/libgmp.a
unix:LIBS += $$_PRO_FILE_PWD_/../../lib/osx/libgmp.a
