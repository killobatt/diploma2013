#include <QString>
#include <QtTest>

#pragma GCC diagnostic ignored "-Wunused-parameter"

#include "linearequationsystem.h"
#include "IntervalNumber/IntervalNumber.h"

class IntervalLinearEquationSystemTest : public QObject
{
    Q_OBJECT
    
public:
    IntervalLinearEquationSystemTest();
      
private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void testCase1();
    void testCase2();
    void testCase3();
    void testCase4();

private:
    template< typename NumberType, typename SizeType >
    LinearEquationSystem< NumberType, SizeType > readSystemFromFile(std::string fileName)
    {
        std::ifstream file(fileName.c_str());

        if (file.is_open())
        {
            std::cout << "Reading file: " << fileName << std::endl;
            uint64_t systemSize;
            file >> systemSize;
            std::cout << "System size: " << systemSize << std::endl;
            Matrix< NumberType, SizeType > matrix(systemSize, systemSize);
            for (SizeType i = 0; i < matrix.rowCount(); i++)
            {
                for (SizeType j = 0; j < matrix.columnCount(); j++)
                {
                    file >> matrix[j][i];
                }
            }

            std::vector< NumberType > freeCoef(systemSize, NumberType(0));
            for (SizeType i = 0; i < freeCoef.size(); i++)
            {
                file >> freeCoef[i];
            }
            return LinearEquationSystem< NumberType, SizeType >(matrix, freeCoef);
        }
        else
        {
            std::cerr << "Error reading file " << fileName.c_str() << std::endl;
        }
        return LinearEquationSystem< NumberType, SizeType >(Matrix< NumberType, SizeType >(0,0), std::vector< NumberType >(0));
    }

    template< typename NumberType, typename SizeType >
    std::vector< NumberType > readSolutionFromFile(std::string fileName)
    {
        std::ifstream file(fileName.c_str());

        if (file.is_open())
        {
            uint64_t systemSize;
            file >> systemSize;
            std::vector< NumberType > solution(systemSize, NumberType(0));
            for (SizeType i = 0; i < solution.size(); i++)
            {
                file >> solution[i];
            }
            return solution;
        }
        else
        {
            std::cerr << "Error reading file " << fileName.c_str() << std::endl;
        }
        return std::vector< NumberType >(0);
    }

    template< typename NumberType >
    void writeSolutionToFile(std::vector< NumberType > solution, std::string fileName)
    {
        std::ofstream file(fileName.c_str());

        if (file.is_open())
        {
            for (size_t i = 0; i < solution.size(); i++)
            {
                file << solution[ i ] << std::endl;
            }
            file.close();
        }
    }

};

IntervalLinearEquationSystemTest::IntervalLinearEquationSystemTest()
{
}

void IntervalLinearEquationSystemTest::initTestCase()
{
}

void IntervalLinearEquationSystemTest::cleanupTestCase()
{
}

void IntervalLinearEquationSystemTest::testCase1()
{
    LinearEquationSystem< IntervalNumber< long double >, uint64_t > linearSystem = readSystemFromFile< IntervalNumber< long double >, uint64_t>("../../input/input01.in");
    std::vector< IntervalNumber< long double > > solution = readSolutionFromFile< IntervalNumber< long double >, uint64_t>("../../output/output01.out");
    std::cout << "System: " << std::endl << linearSystem << std::endl;
    linearSystem.setVerboseOutput(true);
    linearSystem.setVerboseOutputStream(&std::cout);
    linearSystem.solve();
    std::cout << "System: " << std::endl << linearSystem << std::endl;
    std::cout << "Actual solution" << std::endl << linearSystem.solution() << std::endl;
    std::cout << "Expected solution" << std::endl << solution << std::endl;
    QCOMPARE(linearSystem.solution(), solution);
}

void IntervalLinearEquationSystemTest::testCase2()
{
    LinearEquationSystem< IntervalNumber< long double >, uint64_t > linearSystem = readSystemFromFile< IntervalNumber< long double >, uint64_t>("../../input/input02.in");
    std::vector< IntervalNumber< long double > > solution = readSolutionFromFile< IntervalNumber< long double >, uint64_t>("../../output/output02.out");
    std::cout << "System: " << std::endl << linearSystem << std::endl;
    linearSystem.setVerboseOutput(false);
    linearSystem.solve();
    std::cout << "Solved system: " << std::endl << linearSystem << std::endl;
    std::cout << "Actual solution" << std::endl << linearSystem.solution() << std::endl;
    std::cout << "Expected solution" << std::endl << solution << std::endl;
    QCOMPARE(linearSystem.solution(), solution);
}

void IntervalLinearEquationSystemTest::testCase3()
{
//    try
//    {
//        LinearEquationSystem< IntervalNumber< double >, uint64_t > linearSystem = readSystemFromFile< IntervalNumber< double >, uint64_t>("../../input/input03.in");
//        std::cout << "System: " << std::endl << linearSystem << std::endl;
//        linearSystem.setVerboseOutput(false);
//        linearSystem.solve();
//        std::cout << "System: " << std::endl << linearSystem << std::endl;
//    }
//    catch (LinearEquationSystemException *exception)
//    {
//        std::cout << "Catched error: " << exception->what() << std::endl;
//        QCOMPARE(exception->code(), LinearEquationSystemExceptionCode_InconsistentSystem);
//    }
}

void IntervalLinearEquationSystemTest::testCase4()
{
    uint64_t systemSize = 10;

    Matrix< IntervalNumber< long double >, uint64_t > matrix(systemSize, systemSize);
    for (uint32_t i = 0; i < matrix.columnCount(); i++)
    {
        for (uint32_t j = 0; j < matrix.rowCount(); j++)
        {
            long double number = 1.0f / (double)(i + j + 1);
            IntervalNumber< long double > mpq(number);
            matrix[i][j] = mpq;
        }
    }
    std::vector< IntervalNumber< long double > > freeCoef(matrix.rowCount(), IntervalNumber< long double >(1.0L));

    std::ofstream outputStream("../../output/IntervalLinearEquationSystemTest/output04_1.out");

    QElapsedTimer timer;
    LinearEquationSystem< IntervalNumber< long double >, uint64_t > linearSystem(matrix, freeCoef);
    std::cout << "Soving system with size: " << systemSize << std::endl; // << " norm: " << linearSystem.matrix().norm();
    linearSystem.setVerboseOutput(true);
    linearSystem.setVerboseOutputStream(&outputStream);
    timer.start();
    linearSystem.solve();
    std::cout << "Solved system in: " << timer.elapsed() << " ms" << std::endl;
    outputStream.close();

    writeSolutionToFile(linearSystem.solution(), "../../output/IntervalLinearEquationSystemTest/output04.out");
}


QTEST_APPLESS_MAIN(IntervalLinearEquationSystemTest)

#include "intervallinearequationsystemtest.moc"
