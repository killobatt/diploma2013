#-------------------------------------------------
#
# Project created by QtCreator 2013-04-11T00:51:58
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = SLAR
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += include/

win32:LIBS += $$_PRO_FILE_PWD_/lib/win32/libgmp.a
unix:LIBS += $$_PRO_FILE_PWD_/lib/osx/libgmp.a

SOURCES += src/main.cpp \
    src/matrix.cpp \
    src/outputformatting.cpp \
    src/vector.cpp \
    src/linearequationsystem.cpp \
    src/abstractlinearequationsystem.cpp \
    src/basismatrixlinearequationsystem.cpp \
    src/IntervalNumber/intervalnumber.cpp \
    src/IntervalNumber/intervalnumberconfig.cpp \
    src/IntervalNumber/intervalnumberprecision.cpp \
    src/IntervalNumber/wideintervalnumber.cpp \
    src/IntervalNumber/narrowintervalnumber.cpp \
    src/IntervalNumber/intervalnumberimplementation.cpp

#LIBS += /usr/local/lib/libgmp.a

HEADERS += \
    src/matrix.h \
    src/commons.h \
    src/outputformatting.h \
    src/vector.h \
    src/linearequationsystem.h \
    src/abstractlinearequationsystem.h \
    src/basismatrixlinearequationsystem.h \
    src/IntervalNumber/intervalnumber.h \
    src/IntervalNumber/intervalnumberconfig.h \
    src/IntervalNumber/intervalnumberprecision.h \
    src/IntervalNumber/wideintervalnumber.h \
    src/IntervalNumber/narrowintervalnumber.h \
    src/IntervalNumber/intervalnumberimplementation.h
